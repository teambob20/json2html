import unittest

from main import to_string, join_dict, convert_file, parse_tag



class HTML2JSONTestCase(unittest.TestCase):
    maxDiff = None

    def test_join_dict_return_blank_string_on_blank_dict(self):
        self.assertEqual(join_dict({}), '')

    def test_join_dict_return_valid_string_on_valid_dict(self):
        self.assertEqual(
            join_dict({'div': 'text'}),
            '<div>text</div>'
        )

    def test_to_string_returns_valid_string_on_dict_json(self):
        self.assertEqual(
            to_string({'h5': 'data', 'p': 'title'}),
            '<h5>data</h5><p>title</p>'
        )

    def test_to_string_returns_valid_string_on_list_json(self):
        self.assertEqual(
            to_string([{'h5': 'data', 'p': 'title'}, {'div': 'd', 'p': 'paragraph'}]),
            '<ul><li><h5>data</h5><p>title</p></li><li><div>d</div><p>paragraph</p></li></ul>'
        )

    def test_to_string_returns_valid_string_on_complex_json(self):
        # result should be
        '''
            <ul>
                <li>
                    <h5>data</h5>
                    <content>
                        <ul>
                            <li>
                                <h2>title</h2>
                            </li>
                            <li>
                                <list>
                                    <ul>
                                        <li>
                                            <div>content</div>
                                        </li>
                                    </ul>
                                </list>
                            </li>
                        </ul>
                    </content>
                </li>
                <li>
                    <div>d</div>
                    <p>paragraph</p>
                </li>
            </ul>
        '''

        self.assertEqual(
            to_string([
                {'h5': 'data', 'content': [{'h2': 'title'}, {'list': [{'div': 'content'}]}]},
                {'div': 'd', 'p': 'paragraph'}
            ]),
            '<ul><li><h5>data</h5><content><ul><li><h2>title</h2></li><li><list><ul><li><div>content</div></li></ul></list></li></ul></content></li><li><div>d</div><p>paragraph</p></li></ul>'
        )

    def test_to_string_ecapes_html_content(self):
        self.assertEqual(
            to_string({'h5': '<a>data</a>', 'p': 'title'}),
            '<h5>&lt;a&gt;data&lt;/a&gt;</h5><p>title</p>'
        )

    def test_parse_tag_parses_class_attrs(self):
        self.assertEqual(
            parse_tag('p.my-class1.my-class2'),
            ('p class="my-class1 my-class2"', 'p')
        )

    def test_parse_tag_parses_ids_attrs(self):
        self.assertEqual(
            parse_tag('p.my-class1.my-class2#div-id'),
            ('p id="div-id" class="my-class1 my-class2"', 'p')
        )

    def test_parse_tag_parses_id_attr(self):
        self.assertEqual(
            parse_tag('p#div-id'),
            ('p id="div-id"', 'p')
        )
    def test_to_string_not_crashes_on_blank_data(self):
        self.assertEqual(to_string([]), '')

    def test_convert_file_realy_convert_file_data_if_its_in_system(self):
        self.assertIsInstance(convert_file('source.json'), str)

    def test_convert_file_works_if_no_file_in_system(self):
        self.assertIsInstance(convert_file('unexisting_file.json'), str)
