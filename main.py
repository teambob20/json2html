# python-rapidjson or PyPy can be used to speed up processing
# Some of the code(list join_dict) can be splitted to use generators
import json
import os.path
from html import escape


def to_string(data):
    """
    Converts python data structures to equal string

    Returns:
        str
    """
    if not data:
        return ''

    if not isinstance(data, list):
        return join_dict(data)

    return '<ul>{}</ul>'.format(
        ''.join(['<li>{}</li>'.format(join_dict(d)) for d in data])
    )


def join_dict(d):
    """
    Converts dict to the HTML string

    Args:
        d (dict): dict to convert

    Returns:
        str
    """
    result = ''

    for tag, value in d.items():
        # Probably not the best solution with escape flags.
        # But it works and I have no time to find out better solution.
        to_escape = True

        if not isinstance(value, str):
            to_escape = False
            value = to_string(value)

        if to_escape:
            value = escape(value)
        open_tag, close_tag = parse_tag(tag)
        result += '<{open_tag}>{text}</{close_tag}>'.format(
            open_tag=open_tag, text=value, close_tag=close_tag)

    return result


def parse_tag(tag):
    """
    Returns the tuple: tag with tag attributes(classes and ids) and tag

    Args:
        string (str)

    Returns:
        str
    """
    # Definitely not the best solution but the easiest and the most obvious
    # and due to the list comprehensions is not too slow
    # an be rewritten with regex or by char concatenation
    tag, *attrs = tag.split('.')
    result = tag
    ids = []

    if '#' in tag:
        tag, *ids = tag.split('#')
        result = tag

    classes = [x if '#' not in x else x.split('#')[0] for x in attrs]
    ids += [x.split('#')[1] for x in attrs if '#' in x]

    if ids:
        result += ' id="{}"'.format(' '.join(ids))

    if classes:
        result += ' class="{}"'.format(' '.join(classes))

    return result, tag


def convert_file(filename=''):
    """
    Parse file `source.json` and returns simple html with h1 and p tags

    Returns:
        str
    """
    # Separate this logic mostly for tests
    filename = filename or 'source.json'

    if not os.path.isfile(filename):
        return 'Sorry. No such file or dicrectory'

    with open(filename) as file_data:
        return to_string(json.load(file_data))



if __name__ == '__main__':
    print(convert_file())
